
const _ = require('lodash');

module.exports = function () {

	'use strict';

	const exp = new RegExp( '\\b(cruise|airport|helipad|security brief|briefed)\\b', 'ig' );

	this
		.addParameter( ( tokens, callback ) => {

			const outMap = {
				tag: 'services',
				words: {},
			};

			const match = this.cleanSTR.match( exp );

			if ( match )
			{

				_.each( match, ( item ) => {

					if ( this.wordMap[ item ] )
					{
						this.wordMap[ item ].count += 5;
						this.wordMap[ item ].service = true;
					}

					if ( outMap.words[ item ] === undefined )
					{

						outMap.words[ item ] = {
							text: item
						};

					}

				} );

				return callback( null, outMap );

			}

		} );

};