
const _ = require('lodash');

const institutions = [
	'nra',
	'cia',
	'fbi',
	'nsa',
	'dea',
	'onu',
	'otan',
	'opec',
	'fda',
	'dhs',
	'norad',
	'usaf',
	'bank',
	'wall street',
	'kgb',
	'interpool',
	'court',
].join( '|' );

module.exports = function () {

	'use strict';

	const exp = new RegExp( '\\b('+institutions+')\\b', 'ig' );

	this
		.addParameter( ( tokens, callback ) => {

			const outMap = {
				tag: 'institutions',
				words: {},
			};

			const match = this.cleanSTR.match( exp );

			if ( match )
			{

				_.each( match, ( item ) => {

					if ( this.wordMap[ item ] )
					{

						if ( this.wordMap[ item ].stopword )
						{
							return;
						}

						this.wordMap[ item ].count += 15;

						this.wordMap[ item ].institution = true;

					}

					if ( outMap.words[ item ] === undefined )
					{

						outMap.words[ item ] = {
							text: item,
							institution: true,
							count: 1,
						};

					}

				} );

				return callback( null, outMap );

			}

		} );

};
