
const _ = require('lodash');

module.exports = function () {

	'use strict';

	const exp = new RegExp( '\\b(stadium|hotel|home|residence|office|house|building)\\b', 'ig' );

	this
		.addParameter( ( tokens, callback ) => {

			const outMap = {
				tag: 'places',
				words: {},
			};

			const match = this.cleanSTR.match( exp );

			if ( match )
			{

				_.each( match, ( item ) => {

					if ( this.wordMap[ item ] )
					{
						this.wordMap[ item ].count += 10;
						this.wordMap[ item ].place = true;
					}

					if ( outMap.words[ item ] === undefined )
					{

						outMap.words[ item ] = {
							text: item
						};

					}

				} );

				return callback( null, outMap );

			}

		} );

};