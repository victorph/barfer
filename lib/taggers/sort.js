
const _ = require('lodash');

const sort = [
	'sort',
	'alphabetize',
	'arrange',
	'catalogue',
	'categorize',
	'classify',
	'codify',
	'collate',
	'configure',
	'file',
	'grade',
	'group',
	'harmonize',
	'index',
	'integrate',
	'interfile',
	'order',
	'organize',
	'position',
	'rank',
	'register',
	'reorder',
	'reorganize',
	'size',
	'space',
	'systematize',
	'tidy',
	'type',
	'unify',
].join( '|' );

module.exports = function () {

	'use strict';

	const exp = new RegExp( '\\b('+sort+')\\b', 'ig' );

	this
		.addParameter( ( tokens, callback ) => {

			const outMap = {
				tag: 'sort',
				words: {},
			};

			const match = this.cleanSTR.match( exp );

			if ( match )
			{

				_.each( match, ( item ) => {

					if ( this.wordMap[ item ] )
					{
						this.wordMap[ item ].count += 5;
						this.wordMap[ item ].sort = true;
					}

					if ( outMap.words[ item ] === undefined )
					{

						outMap.words[ item ] = {
							text: item,
							count: 1,
							sort: true,
						};

					}

				} );

				return callback( null, outMap );

			}

		} );

};
