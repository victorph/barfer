
const _ = require('lodash');

const vehicles = [
	'subway',
	'patineta',
	'skateboard',
	'carro',
	'avion',
	'bici',
	'bicicleta',
	'helicoptero',
	'helicopter',
	'tren',
	'metro',
	'bus',
	'micro',
	'ferrocarril',
	'car',
	'plane',
	'boat',
	'taxi',
	'bike',
	'bicycle',
].join( '|' );

module.exports = function () {

	'use strict';

	const exp = new RegExp( '\\b('+vehicles+')\\b', 'ig' );

	this
		.addParameter( ( tokens, callback ) => {

			const match = this.cleanSTR.match( exp );

			if ( match )
			{

				if ( this.wordMap[ match[ 0 ] ] )
				{
					this.wordMap[ match[ 0 ] ].count += 5;
					this.wordMap[ match[ 0 ] ].vehicle = true;
				}

				return callback( null, {
					tag: 'vehicles',
					text: match[ 0 ],
				} );

			}

		} );

};