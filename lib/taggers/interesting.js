
const _ = require('lodash');

module.exports = function () {

	'use strict';

	if ( this.interesting !== false )
	{

		let TOKENS;

		if ( this.interesting.constructor === String )
		{
			TOKENS = this.interesting.split( ',' ).join( '|' );
		}
		else if ( this.interesting.constructor === Array )
		{
			TOKENS = this.interesting.join( '|' );
		}
		else if ( this.interesting )
		{
			throw new Error( 'interesting is not String or Array' );
		}

		const exp = new RegExp( `\\b(${TOKENS})\\b`, 'ig' );

		this
			.addParameter( ( tokens, callback ) => {

				const outMap = {
					tag: 'interesting',
					words: {},
				};

				let str = this
					.cleanSTR
					.replace( '\n', '' );

				if ( this.latinize )
				{
					str = str.latinize();
				}

				const match = str.match( exp );

				if ( match )
				{

					_.each( match, ( item ) => {

						if ( this.wordMap[ item ] )
						{
							this.wordMap[ item ].count += 30;
							this.wordMap[ item ].interesting = true;
						}

						if ( outMap.words[ item ] === undefined )
						{

							outMap.words[ item ] = {
								text: item
							};

						}

					} );

					return callback( null, outMap );

				}

			} );

	}

};