
const _ = require('lodash');
const async = require('async');

module.exports = function () {

	'use strict';

	this
		.addParameter( ( tokens, callback ) => {

			const outMap =
			{
				tag: 'topics',
				words: {},
			};

			async
				.each( this.wordMap, ( word, next ) => {

					if ( word.length < 3 || word.count < 2 || word.text === '&amp' )
					{
						return next();
					}

					if ( this.twitter === false )
					{

						if ( word.text.indexOf( '@' ) === 0 )
						{
							return next();
						}
						else if ( word.text.indexOf( '#' ) === 0 )
						{
							return next();
						}
						else if ( word.text.indexOf( '$' ) === 0 )
						{
							return next();
						}

					}

					if ( outMap.words[ word.text ] === undefined )
					{

						outMap.words[ word.text ] = word;

					}

					if ( this.wordMap[ word.text ] )
					{

						let value = 20;

						if ( this.twitter === true )
						{

							if ( word.mention )
							{
								value += 9;
							}

							if ( word.hashtag )
							{
								value += 8;
							}

						}

						if ( word.sentiment )
						{
							value += 8;
						}

						if ( word.interesting )
						{
							value += 8;
						}

						if ( word.action )
						{
							value += 8;
						}

						if ( word.rest )
						{
							value -= 6;
						}

						if ( word.stopword )
						{
							value -= 6;
						}

						if ( value < 15 ) { return next(); }

						this.wordMap[ word.text ].weight += _.floor( value / ( word.count + 3 ), 2 );

						this.wordMap[ word.text ].topic = true;

						this.wordMap[ word.text ].count += 5;

					}

					return next();

				}, () => callback( null, outMap ) );

		} );

};