
const _ = require('lodash');

const resume = [
	'begin',
	'begin over',
	'carry',
	'contin',
	'forge ahead',
	'go on',
	'keep moving',
	'pick up',
	'proceed',
	'progress',
	'recapitulat',
	'recommenc',
	'reestablish',
	'reinstat',
	'renew',
	'reopen',
	'restor',
	'return to',
	'return',
	'take up',
	'take on',
].join( '|' );

module.exports = function () {

	'use strict';

	const exp = new RegExp( '\\b('+resume+')\\b', 'ig' );

	this
		.addParameter( ( tokens, callback ) => {

			const outMap = {
				tag: 'resume',
				words: {},
			};

			const match = this.cleanSTR.match( exp );

			if ( match )
			{

				_.each( match, ( item ) => {

					if ( this.wordMap[ item ] )
					{
						this.wordMap[ item ].count += 5;
						this.wordMap[ item ].resume = true;
					}

					if ( outMap.words[ item ] === undefined )
					{

						outMap.words[ item ] = {
							text: item,
							count: 1,
							resume: true,
						};

					}

				} );

				return callback( null, outMap );

			}

		} );

};