
const async = require('async');

const hesitate = [
	'alternat',
	'irresolut',
	'uncertain',
	'dont know',
	'don\'t know',
	'dith',
	'falt',
	'hang back',
	'shift',
	'switch',
	'vacillat',
	'wag',
	'wave',
	'unsur',
].join( '|' );

module.exports = function () {

	'use strict';

	const exp = new RegExp( '\\b('+hesitate+')\\b', 'ig' );

	this
		.addParameter( ( tokens, callback ) => {

			const outMap = {
				tag: 'hesitate',
				words: {},
			};

			const match = this.cleanSTR.match( exp );

			if ( match )
			{

				async
					.each( match, ( item, next ) => {

						if ( this.wordMap[ item ] )
						{
							this.wordMap[ item ].count += 5;
							this.wordMap[ item ].weight += 0.15;
							this.wordMap[ item ].hesitate = true;
						}

						if ( outMap.words[ item ] === undefined )
						{

							outMap.words[ item ] = {
								text: item,
								count: 1,
								weight: 0.15,
								hesitate: true,
							};

						}

						return next();

					}, () => callback( null, outMap ) );

			}

		} );

};