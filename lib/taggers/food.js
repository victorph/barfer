
const _ = require('lodash');

module.exports = function () {

	'use strict';

	const exp = new RegExp( '\\b(tacos|papas|torta|helado|carne|dona|cafe|pizza|sushi|tacos|(burgers|chees burgers)|angus|beef|bbq|icecream|donnut|coffe)\\b', 'ig' );

	this
		.addParameter( ( tokens ) => {

			const outMap = {
				tag: 'foods',
				words: {},
			};

			const match = this.cleanSTR.match( exp );

			if ( match && match.index === 0 )
			{

				_.each( match, ( item ) => {

					if ( this.wordMap[ item ] )
					{
						this.wordMap[ item ].count += 2;
						this.wordMap[ item ].food = true;
					}

					if ( outMap.words[ item ] === undefined )
					{

						outMap.words[ item ] = {
							text: item
						};

					}

				} );

				return callback( null, outMap );

			}

		} );

};