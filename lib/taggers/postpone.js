
const _ = require('lodash');

const postpone = [
		'adjourn',
		'dally',
		'defer',
		'delay',
		'hold off',
		'hold over',
		'hold up on',
		'lay over',
		'linger',
		'take time',
		'mull',
		'play for time',
		'prorogue',
		'put off',
		'put on hold',
		'put on ice',
		'put over',
		'remit',
		'respite',
		'shelve',
		'stall',
		'stay',
		'stonewall',
		'suspend',
		'take one\'s time',
		'tarry',
		'temporiz',
	].join( '|' );

module.exports = function () {

	'use strict';

	const exp = new RegExp( '\\b('+postpone+')\\b', 'ig' );

	this
		.addParameter( ( tokens, callback ) => {

			const outMap = {
				tag: 'postpone',
				words: {},
			};

			const match = this.cleanSTR.match( exp );

			if ( match )
			{

				_.each( match, ( item ) => {

					if ( this.wordMap[ item ] )
					{
						this.wordMap[ item ].count += 5;
						this.wordMap[ item ].postpone = true;
					}

					if ( outMap.words[ item ] === undefined )
					{

						outMap.words[ item ] = {
							text: item,
							count: 1,
							postpone: true,
						};

					}

				} );

				return callback( null, outMap );

			}

		} );

};