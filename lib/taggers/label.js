
const _ = require('lodash');

const label = [
	'label',
	'brand',
	'call',
	'christen',
	'define',
	'denominate',
	'designate',
	'determine',
	'diagnose',
	'dub',
	'flag',
	'identify',
	'imprint',
	'price',
	'mark',
	'name',
	'note',
	'sign',
	'stamp',
	'tab',
	'tag',
	'tally',
	'term',
	'ticket',
	'title',
].join( '|' );

module.exports = function () {

	'use strict';

	const exp = new RegExp( '\\b('+label+')\\b', 'ig' );

	this
		.addParameter( ( tokens, callback ) => {

			const outMap = {
				tag: 'label',
				words: {},
			};

			const match = this.cleanSTR.match( exp );

			if ( match )
			{

				_.each( match, ( item ) => {

					if ( this.wordMap[ item ] )
					{
						this.wordMap[ item ].count += 5;
						this.wordMap[ item ].label = true;
					}

					if ( outMap.words[ item ] === undefined )
					{

						outMap.words[ item ] = {
							text: item,
							count: 1,
							label: true,
						};

					}

				} );

				return callback( null, outMap );

			}

		} );

};