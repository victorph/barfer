
const _ = require('lodash');

module.exports = function () {

	'use strict';

	if ( this.target !== false )
	{

		const target = this.target;

		const exp = new RegExp( `\\b(${target})\\b`, 'i' );

		this
			.addParameter( ( tokens, callback ) => {

				const outMap = {
					tag: 'target',
					words: {},
				};

				const match = tokens.text.match( exp );

				if ( match )
				{

					_.each( match, ( item ) => {

						if ( this.wordMap[ item ] )
						{

							this.wordMap[ item ].count += 10;

							this.wordMap[ item ].target = true;

						}

						if ( outMap.words[ item ] === undefined )
						{

							outMap.words[ item ] = {
								text: item,
								data: match
							};

						}

					} );

					return callback( null, outMap );

				}

			} );

	}

};