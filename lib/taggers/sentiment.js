
const _ = require('lodash');

module.exports = function () {

	'use strict';

	let positive = '';
	let negative = '';

	_.each( this.lang.whitelist, ( item ) => {

		if ( negative.length > 0 )
		{
			negative += '|';
		}

		if ( positive.length > 0 )
		{
			positive += '|';
		}

		if ( this.afinn[ item ] )
		{

			_.each( this.afinn[ item ], ( val, key ) => {

				if ( key.length < 3 ) { return; }

				if ( val > 0 )
				{
					positive += `${key}(\w){0,2}|`;
				}

				if ( val < 0 )
				{
					negative += `${key}(\w){0,2}|`;
				}

			} );

		}

	} );

	positive = positive.substr( 0, positive.length - 1 );
	negative = negative.substr( 0, negative.length - 1 );

	const expPostivie = new RegExp( `\\b(${positive})\\b`, 'ig' );

	const expNegative = new RegExp( `\\b(${negative})\\b`, 'ig' );

	this
		.addParameter( ( tokens, callback ) => {

			const outMap = {
				tag: 'positive',
				words: {},
			};

			const match = tokens.text.match( expPostivie );

			if ( match )
			{

				if ( match.index > 0 ) { return; }

				_.each( match, ( item ) => {

					if ( item.length < 3 ) { return; }

					const splt = tokens.text.split( item );
					const before = splt[ 0 ].split( ' ' );
					const after = splt[ 1 ].split( ' ' );

					if ( outMap.words[ item ] )
					{
						outMap.words[ item ].data = _.compact( outMap.words[ item ].data.concat( before.concat( after ) ) );
						outMap.words[ item ].data = _.uniq( outMap.words[ item ].data );
					}
					else
					{

						outMap.words[ item ] = {
							text: item,
							data: _.compact( before.concat( after ) ),
						};

					}

					if ( this.wordMap[ item ] )
					{
						this.wordMap[ item ].count += 20;
						this.wordMap[ item ].sentiment = true;
						this.wordMap[ item ].positive = true;
					}

				} );

			}

			callback( null, outMap );

		} );

	this
		.addParameter( ( tokens, callback ) => {

			const outMap = {
				tag: 'negative',
				words: {},
			};

			const match = tokens.text.match( expNegative );

			if ( match )
			{

				if ( match.index > 0 ) { return; }

				_.each( match, ( item ) => {

					if ( item.length < 3 ) { return; }

					const splt = tokens.text.split( item );
					const before = splt[ 0 ].split( ' ' );
					const after = splt[ 1 ].split( ' ' );

					if ( outMap.words[ item ] )
					{
						outMap.words[ item ].data = _.compact( outMap.words[ item ].data.concat( before.concat( after ) ) );
						outMap.words[ item ].data = _.uniq( outMap.words[ item ].data );
					}
					else
					{

						outMap.words[ item ] = {
							text: item,
							data: _.compact( before.concat( after ) ),
						};

					}

					if ( this.wordMap[ item ] )
					{
						this.wordMap[ item ].count += 20;
						this.wordMap[ item ].sentiment = true;
						this.wordMap[ item ].negative = true;
					}

				} );

			}

			callback( null, outMap );

		} );

};