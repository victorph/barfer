const _ = require('lodash');
const async = require('async');

const fs = require('fs');
const path = require('path');

const natural = require('natural');
const tokenizer = new natural.WordPunctTokenizer();

const twitterTX = require('twitter-text');

const gemoji = require('gemoji');

const franc = require('franc');

const gender = require('gender-guess');

const emojiEmotion = require( 'emoji-emotion' );

const LanguageDetect = require('languagedetect');
const lngDetector = new LanguageDetect();

_.each( gemoji.unicode, ( item, key ) => {

	if ( item.names[ 0 ] === 'rose' || item.names[ 0 ] === 'smiley_cat' || item.names[ 0 ] === 'smile_cat' || item.names[ 0 ] === 'joy_cat' || item.names[ 0 ] === 'heart_eyes_cat' || item.names[ 0 ] === 'mortar_board' || item.names[ 0 ] === 'four_leaf_clover' || item.names[ 0 ] === 'shamrock' || item.names[ 0 ] === 'peace_symbol' || item.names[ 0 ] === 'blue_heart' || item.names[ 0 ] === 'yellow_heart' || item.names[ 0 ] === 'green_heart' || item.names[ 0 ] === 'purple_heart' || item.names[ 0 ] === 'two_hearts' || item.names[ 0 ] === 'heartbeat' || item.names[ 0 ] === 'heartpulse' )
	{

		emojiEmotion.push( {
			emoji: key,
			polarity: 1,
		} );

	}

	if ( item.names[ 0 ] === 'skull' || item.names[ 0 ] === 'pouting_cat' || item.names[ 0 ] === 'crying_cat_face' || item.names[ 0 ] === 'scream_cat' || item.names[ 0 ] === 'biohazard' || item.names[ 0 ] === 'x' || item.names[ 0 ] === 'warning' || item.names[ 0 ] === 'sos' || item.names[ 0 ] === 'anger' )
	{

		emojiEmotion.push( {
			emoji: key,
			polarity: -1,
		} );

	}

} );

/* jshint ignore:start */
const Latinise={};Latinise.latin_map={"Á":"A","Ă":"A","Ắ":"A","Ặ":"A","Ằ":"A","Ẳ":"A","Ẵ":"A","Ǎ":"A","Â":"A","Ấ":"A","Ậ":"A","Ầ":"A","Ẩ":"A","Ẫ":"A","Ä":"A","Ǟ":"A","Ȧ":"A","Ǡ":"A","Ạ":"A","Ȁ":"A","À":"A","Ả":"A","Ȃ":"A","Ā":"A","Ą":"A","Å":"A","Ǻ":"A","Ḁ":"A","Ⱥ":"A","Ã":"A","Ꜳ":"AA","Æ":"AE","Ǽ":"AE","Ǣ":"AE","Ꜵ":"AO","Ꜷ":"AU","Ꜹ":"AV","Ꜻ":"AV","Ꜽ":"AY","Ḃ":"B","Ḅ":"B","Ɓ":"B","Ḇ":"B","Ƀ":"B","Ƃ":"B","Ć":"C","Č":"C","Ç":"C","Ḉ":"C","Ĉ":"C","Ċ":"C","Ƈ":"C","Ȼ":"C","Ď":"D","Ḑ":"D","Ḓ":"D","Ḋ":"D","Ḍ":"D","Ɗ":"D","Ḏ":"D","ǲ":"D","ǅ":"D","Đ":"D","Ƌ":"D","Ǳ":"DZ","Ǆ":"DZ","É":"E","Ĕ":"E","Ě":"E","Ȩ":"E","Ḝ":"E","Ê":"E","Ế":"E","Ệ":"E","Ề":"E","Ể":"E","Ễ":"E","Ḙ":"E","Ë":"E","Ė":"E","Ẹ":"E","Ȅ":"E","È":"E","Ẻ":"E","Ȇ":"E","Ē":"E","Ḗ":"E","Ḕ":"E","Ę":"E","Ɇ":"E","Ẽ":"E","Ḛ":"E","Ꝫ":"ET","Ḟ":"F","Ƒ":"F","Ǵ":"G","Ğ":"G","Ǧ":"G","Ģ":"G","Ĝ":"G","Ġ":"G","Ɠ":"G","Ḡ":"G","Ǥ":"G","Ḫ":"H","Ȟ":"H","Ḩ":"H","Ĥ":"H","Ⱨ":"H","Ḧ":"H","Ḣ":"H","Ḥ":"H","Ħ":"H","Í":"I","Ĭ":"I","Ǐ":"I","Î":"I","Ï":"I","Ḯ":"I","İ":"I","Ị":"I","Ȉ":"I","Ì":"I","Ỉ":"I","Ȋ":"I","Ī":"I","Į":"I","Ɨ":"I","Ĩ":"I","Ḭ":"I","Ꝺ":"D","Ꝼ":"F","Ᵹ":"G","Ꞃ":"R","Ꞅ":"S","Ꞇ":"T","Ꝭ":"IS","Ĵ":"J","Ɉ":"J","Ḱ":"K","Ǩ":"K","Ķ":"K","Ⱪ":"K","Ꝃ":"K","Ḳ":"K","Ƙ":"K","Ḵ":"K","Ꝁ":"K","Ꝅ":"K","Ĺ":"L","Ƚ":"L","Ľ":"L","Ļ":"L","Ḽ":"L","Ḷ":"L","Ḹ":"L","Ⱡ":"L","Ꝉ":"L","Ḻ":"L","Ŀ":"L","Ɫ":"L","ǈ":"L","Ł":"L","Ǉ":"LJ","Ḿ":"M","Ṁ":"M","Ṃ":"M","Ɱ":"M","Ń":"N","Ň":"N","Ņ":"N","Ṋ":"N","Ṅ":"N","Ṇ":"N","Ǹ":"N","Ɲ":"N","Ṉ":"N","Ƞ":"N","ǋ":"N","Ñ":"N","Ǌ":"NJ","Ó":"O","Ŏ":"O","Ǒ":"O","Ô":"O","Ố":"O","Ộ":"O","Ồ":"O","Ổ":"O","Ỗ":"O","Ö":"O","Ȫ":"O","Ȯ":"O","Ȱ":"O","Ọ":"O","Ő":"O","Ȍ":"O","Ò":"O","Ỏ":"O","Ơ":"O","Ớ":"O","Ợ":"O","Ờ":"O","Ở":"O","Ỡ":"O","Ȏ":"O","Ꝋ":"O","Ꝍ":"O","Ō":"O","Ṓ":"O","Ṑ":"O","Ɵ":"O","Ǫ":"O","Ǭ":"O","Ø":"O","Ǿ":"O","Õ":"O","Ṍ":"O","Ṏ":"O","Ȭ":"O","Ƣ":"OI","Ꝏ":"OO","Ɛ":"E","Ɔ":"O","Ȣ":"OU","Ṕ":"P","Ṗ":"P","Ꝓ":"P","Ƥ":"P","Ꝕ":"P","Ᵽ":"P","Ꝑ":"P","Ꝙ":"Q","Ꝗ":"Q","Ŕ":"R","Ř":"R","Ŗ":"R","Ṙ":"R","Ṛ":"R","Ṝ":"R","Ȑ":"R","Ȓ":"R","Ṟ":"R","Ɍ":"R","Ɽ":"R","Ꜿ":"C","Ǝ":"E","Ś":"S","Ṥ":"S","Š":"S","Ṧ":"S","Ş":"S","Ŝ":"S","Ș":"S","Ṡ":"S","Ṣ":"S","Ṩ":"S","Ť":"T","Ţ":"T","Ṱ":"T","Ț":"T","Ⱦ":"T","Ṫ":"T","Ṭ":"T","Ƭ":"T","Ṯ":"T","Ʈ":"T","Ŧ":"T","Ɐ":"A","Ꞁ":"L","Ɯ":"M","Ʌ":"V","Ꜩ":"TZ","Ú":"U","Ŭ":"U","Ǔ":"U","Û":"U","Ṷ":"U","Ü":"U","Ǘ":"U","Ǚ":"U","Ǜ":"U","Ǖ":"U","Ṳ":"U","Ụ":"U","Ű":"U","Ȕ":"U","Ù":"U","Ủ":"U","Ư":"U","Ứ":"U","Ự":"U","Ừ":"U","Ử":"U","Ữ":"U","Ȗ":"U","Ū":"U","Ṻ":"U","Ų":"U","Ů":"U","Ũ":"U","Ṹ":"U","Ṵ":"U","Ꝟ":"V","Ṿ":"V","Ʋ":"V","Ṽ":"V","Ꝡ":"VY","Ẃ":"W","Ŵ":"W","Ẅ":"W","Ẇ":"W","Ẉ":"W","Ẁ":"W","Ⱳ":"W","Ẍ":"X","Ẋ":"X","Ý":"Y","Ŷ":"Y","Ÿ":"Y","Ẏ":"Y","Ỵ":"Y","Ỳ":"Y","Ƴ":"Y","Ỷ":"Y","Ỿ":"Y","Ȳ":"Y","Ɏ":"Y","Ỹ":"Y","Ź":"Z","Ž":"Z","Ẑ":"Z","Ⱬ":"Z","Ż":"Z","Ẓ":"Z","Ȥ":"Z","Ẕ":"Z","Ƶ":"Z","Ĳ":"IJ","Œ":"OE","ᴀ":"A","ᴁ":"AE","ʙ":"B","ᴃ":"B","ᴄ":"C","ᴅ":"D","ᴇ":"E","ꜰ":"F","ɢ":"G","ʛ":"G","ʜ":"H","ɪ":"I","ʁ":"R","ᴊ":"J","ᴋ":"K","ʟ":"L","ᴌ":"L","ᴍ":"M","ɴ":"N","ᴏ":"O","ɶ":"OE","ᴐ":"O","ᴕ":"OU","ᴘ":"P","ʀ":"R","ᴎ":"N","ᴙ":"R","ꜱ":"S","ᴛ":"T","ⱻ":"E","ᴚ":"R","ᴜ":"U","ᴠ":"V","ᴡ":"W","ʏ":"Y","ᴢ":"Z","á":"a","ă":"a","ắ":"a","ặ":"a","ằ":"a","ẳ":"a","ẵ":"a","ǎ":"a","â":"a","ấ":"a","ậ":"a","ầ":"a","ẩ":"a","ẫ":"a","ä":"a","ǟ":"a","ȧ":"a","ǡ":"a","ạ":"a","ȁ":"a","à":"a","ả":"a","ȃ":"a","ā":"a","ą":"a","ᶏ":"a","ẚ":"a","å":"a","ǻ":"a","ḁ":"a","ⱥ":"a","ã":"a","ꜳ":"aa","æ":"ae","ǽ":"ae","ǣ":"ae","ꜵ":"ao","ꜷ":"au","ꜹ":"av","ꜻ":"av","ꜽ":"ay","ḃ":"b","ḅ":"b","ɓ":"b","ḇ":"b","ᵬ":"b","ᶀ":"b","ƀ":"b","ƃ":"b","ɵ":"o","ć":"c","č":"c","ç":"c","ḉ":"c","ĉ":"c","ɕ":"c","ċ":"c","ƈ":"c","ȼ":"c","ď":"d","ḑ":"d","ḓ":"d","ȡ":"d","ḋ":"d","ḍ":"d","ɗ":"d","ᶑ":"d","ḏ":"d","ᵭ":"d","ᶁ":"d","đ":"d","ɖ":"d","ƌ":"d","ı":"i","ȷ":"j","ɟ":"j","ʄ":"j","ǳ":"dz","ǆ":"dz","é":"e","ĕ":"e","ě":"e","ȩ":"e","ḝ":"e","ê":"e","ế":"e","ệ":"e","ề":"e","ể":"e","ễ":"e","ḙ":"e","ë":"e","ė":"e","ẹ":"e","ȅ":"e","è":"e","ẻ":"e","ȇ":"e","ē":"e","ḗ":"e","ḕ":"e","ⱸ":"e","ę":"e","ᶒ":"e","ɇ":"e","ẽ":"e","ḛ":"e","ꝫ":"et","ḟ":"f","ƒ":"f","ᵮ":"f","ᶂ":"f","ǵ":"g","ğ":"g","ǧ":"g","ģ":"g","ĝ":"g","ġ":"g","ɠ":"g","ḡ":"g","ᶃ":"g","ǥ":"g","ḫ":"h","ȟ":"h","ḩ":"h","ĥ":"h","ⱨ":"h","ḧ":"h","ḣ":"h","ḥ":"h","ɦ":"h","ẖ":"h","ħ":"h","ƕ":"hv","í":"i","ĭ":"i","ǐ":"i","î":"i","ï":"i","ḯ":"i","ị":"i","ȉ":"i","ì":"i","ỉ":"i","ȋ":"i","ī":"i","į":"i","ᶖ":"i","ɨ":"i","ĩ":"i","ḭ":"i","ꝺ":"d","ꝼ":"f","ᵹ":"g","ꞃ":"r","ꞅ":"s","ꞇ":"t","ꝭ":"is","ǰ":"j","ĵ":"j","ʝ":"j","ɉ":"j","ḱ":"k","ǩ":"k","ķ":"k","ⱪ":"k","ꝃ":"k","ḳ":"k","ƙ":"k","ḵ":"k","ᶄ":"k","ꝁ":"k","ꝅ":"k","ĺ":"l","ƚ":"l","ɬ":"l","ľ":"l","ļ":"l","ḽ":"l","ȴ":"l","ḷ":"l","ḹ":"l","ⱡ":"l","ꝉ":"l","ḻ":"l","ŀ":"l","ɫ":"l","ᶅ":"l","ɭ":"l","ł":"l","ǉ":"lj","ſ":"s","ẜ":"s","ẛ":"s","ẝ":"s","ḿ":"m","ṁ":"m","ṃ":"m","ɱ":"m","ᵯ":"m","ᶆ":"m","ń":"n","ň":"n","ņ":"n","ṋ":"n","ȵ":"n","ṅ":"n","ṇ":"n","ǹ":"n","ɲ":"n","ṉ":"n","ƞ":"n","ᵰ":"n","ᶇ":"n","ɳ":"n","ñ":"n","ǌ":"nj","ó":"o","ŏ":"o","ǒ":"o","ô":"o","ố":"o","ộ":"o","ồ":"o","ổ":"o","ỗ":"o","ö":"o","ȫ":"o","ȯ":"o","ȱ":"o","ọ":"o","ő":"o","ȍ":"o","ò":"o","ỏ":"o","ơ":"o","ớ":"o","ợ":"o","ờ":"o","ở":"o","ỡ":"o","ȏ":"o","ꝋ":"o","ꝍ":"o","ⱺ":"o","ō":"o","ṓ":"o","ṑ":"o","ǫ":"o","ǭ":"o","ø":"o","ǿ":"o","õ":"o","ṍ":"o","ṏ":"o","ȭ":"o","ƣ":"oi","ꝏ":"oo","ɛ":"e","ᶓ":"e","ɔ":"o","ᶗ":"o","ȣ":"ou","ṕ":"p","ṗ":"p","ꝓ":"p","ƥ":"p","ᵱ":"p","ᶈ":"p","ꝕ":"p","ᵽ":"p","ꝑ":"p","ꝙ":"q","ʠ":"q","ɋ":"q","ꝗ":"q","ŕ":"r","ř":"r","ŗ":"r","ṙ":"r","ṛ":"r","ṝ":"r","ȑ":"r","ɾ":"r","ᵳ":"r","ȓ":"r","ṟ":"r","ɼ":"r","ᵲ":"r","ᶉ":"r","ɍ":"r","ɽ":"r","ↄ":"c","ꜿ":"c","ɘ":"e","ɿ":"r","ś":"s","ṥ":"s","š":"s","ṧ":"s","ş":"s","ŝ":"s","ș":"s","ṡ":"s","ṣ":"s","ṩ":"s","ʂ":"s","ᵴ":"s","ᶊ":"s","ȿ":"s","ɡ":"g","ᴑ":"o","ᴓ":"o","ᴝ":"u","ť":"t","ţ":"t","ṱ":"t","ț":"t","ȶ":"t","ẗ":"t","ⱦ":"t","ṫ":"t","ṭ":"t","ƭ":"t","ṯ":"t","ᵵ":"t","ƫ":"t","ʈ":"t","ŧ":"t","ᵺ":"th","ɐ":"a","ᴂ":"ae","ǝ":"e","ᵷ":"g","ɥ":"h","ʮ":"h","ʯ":"h","ᴉ":"i","ʞ":"k","ꞁ":"l","ɯ":"m","ɰ":"m","ᴔ":"oe","ɹ":"r","ɻ":"r","ɺ":"r","ⱹ":"r","ʇ":"t","ʌ":"v","ʍ":"w","ʎ":"y","ꜩ":"tz","ú":"u","ŭ":"u","ǔ":"u","û":"u","ṷ":"u","ü":"u","ǘ":"u","ǚ":"u","ǜ":"u","ǖ":"u","ṳ":"u","ụ":"u","ű":"u","ȕ":"u","ù":"u","ủ":"u","ư":"u","ứ":"u","ự":"u","ừ":"u","ử":"u","ữ":"u","ȗ":"u","ū":"u","ṻ":"u","ų":"u","ᶙ":"u","ů":"u","ũ":"u","ṹ":"u","ṵ":"u","ᵫ":"ue","ꝸ":"um","ⱴ":"v","ꝟ":"v","ṿ":"v","ʋ":"v","ᶌ":"v","ⱱ":"v","ṽ":"v","ꝡ":"vy","ẃ":"w","ŵ":"w","ẅ":"w","ẇ":"w","ẉ":"w","ẁ":"w","ⱳ":"w","ẘ":"w","ẍ":"x","ẋ":"x","ᶍ":"x","ý":"y","ŷ":"y","ÿ":"y","ẏ":"y","ỵ":"y","ỳ":"y","ƴ":"y","ỷ":"y","ỿ":"y","ȳ":"y","ẙ":"y","ɏ":"y","ỹ":"y","ź":"z","ž":"z","ẑ":"z","ʑ":"z","ⱬ":"z","ż":"z","ẓ":"z","ȥ":"z","ẕ":"z","ᵶ":"z","ᶎ":"z","ʐ":"z","ƶ":"z","ɀ":"z","ﬀ":"ff","ﬃ":"ffi","ﬄ":"ffl","ﬁ":"fi","ﬂ":"fl","ĳ":"ij","œ":"oe","ﬆ":"st","ₐ":"a","ₑ":"e","ᵢ":"i","ⱼ":"j","ₒ":"o","ᵣ":"r","ᵤ":"u","ᵥ":"v","ₓ":"x"};
String.prototype.latinise=function(){return this.replace(/[^A-Za-z0-9\[\] ]/g,function(a){return Latinise.latin_map[a]||a})};
String.prototype.latinize=String.prototype.latinise;
// String.prototype.isLatin=function(){return this==this.latinise()}
/* jshint ignore:end */

const defaultConf = {

	twitter: true,

	franc: true,

	lang:
	{
		whitelist: [ 'eng', 'spa', 'por', 'fra', 'ger' ]
	},

};

class Barfer {

	constructor ( params ) {

		'use strict';

		this.twitter = false;
		this.target = false;
		this.interesting = false;

		this.wordMap = {};

		this.parameters = [];

		this.tagger = {};

		_.each( defaultConf, ( value, key ) => this[ key ] = value );

		this.afinn = {
				'spa': require( './afinnES'),
				'eng': require( 'afinn-111' ),
			};

		this.plugins = {
				study: [],
				string: [],
				tagger: [],
			};

		if ( params.afinn && params.afinn.constructor === Object )
		{
			_.each( params.afinn, ( list, iso3 ) => this.afinn[ iso3 ] = list );
		}

		if ( params.clean )
		{
			this.clean = params.clean;
		}

		if ( params.latinize )
		{
			this.latinize = params.latinize;
		}

		if ( params.twitter )
		{
			this.twitter = params.twitter;
		}

		if ( params.franc )
		{
			this.franc = params.franc;
		}

		if ( params.languagedetect )
		{
			this.languagedetect = params.languagedetect;
		}

		if ( params.lang )
		{
			this.lang = params.lang;
		}

		if ( params.target )
		{
			this.target = params.target.toLowerCase();
		}

		if ( params.interesting )
		{
			this.interesting = params.interesting.toLowerCase();
		}

		if ( params.plugins )
		{

			if ( params.plugins.constructor === Array )
			{

				_.each( params.plugins, ( plugin ) => {

					if ( ! plugin.type || ! plugin.logic )
					{
						throw new Error( 'Plugin is not in a invalid format, it needs a type (string) and a logic (function) paths defined.' );
					}

					this.plugins[ plugin.type ].push( plugin.logic );

				} );

			}

		}

		// Load all default tags
		_.each( [
			'action',
			'sentiment',
			'postpone',
			'resume',
			'hesitate',
			'label',
			'sort',
			'interesting',
			'target',
			'institutions',
			'places',
			'vehicle',
			'services',
			'food',
			'topic',
		], ( defaultTagger ) => require( `./taggers/${defaultTagger}.js` ).call( this ) );

		// Call tagger plugins
		_.each( this.plugins.tagger, ( pluginTagger ) => pluginTagger.call( this ) );

	}

	itemStem ( item ) {

		'use strict';

		const portStem = natural
			.PorterStemmer
			.stem( item );

		const lancStem = natural
			.LancasterStemmer
			.stem( item );

		if ( portStem.length > lancStem.length )
		{
			return portStem;
		}

		return lancStem;

	}

	stringClean ( str ) {

		'use strict';

		str = str
			.replace( /pic.twitter.com\/\w*/igm, ' ' )
			.replace( /(?:https?|ftp):\/\/[\n\S]+/igm, ' ')
			.replace( /htt|http|https:|https:\/\…|https:…|https /igm, ' ' )
			.replace( '\n', ' ' )
			.replace( /\“|\"|\¿|\!|\,|\.|\?|\:|\;|\…|\¡|\*|\'|\/|\)|\(|\]|\[|\-|&amp;/igm, ' ' )
			.replace( /(\s){2,5}/igm, ' ' );

		if ( this.latinize )
		{
			str.latinize();
		}

		return str;

	}

	stringProcess ( res, complete ) {

		'use strict';

		let stopwords = require( 'stopwords-en' );

		if ( this.lang && this.lang.guess === 'spa' )
		{
			stopwords = require( 'stopwords-es' );
		}

		this.originalSTR = res.str;

		if ( this.clean )
		{
			this.cleanSTR = this.stringClean( res.str );
		}
		else
		{
			this.cleanSTR = res.str;
		}

		this.topics = [];

		this.rest = [];

		this.stopwords = [];

		this.trigrams = [];

		this.tokens = [];

		this.stems = [];

		// Process trigrams with the filtered tokens
		this.trigrams = natural.NGrams.trigrams( this.cleanSTR );

		// Use those trigrams to call the default filters
		async.each( this.trigrams, ( ar, done ) => {

			async.waterfall( [

				( complete ) => {

					async.each( ar, ( token, next ) => {

						// check token ;)
						if ( token.length < 2 ) { return; }

						// stem token
						const stem = this.itemStem( token );

						// add token to wordMap
						if ( this.wordMap[ token ] === undefined )
						{

							this.wordMap[ token ] = {
								count: 1,
								length: token.length,
								stem,
								text: token,
								weight: 0.1,
							};

							if ( this.target && this.target.indexOf( token ) > -1 )
							{
								this.wordMap[ token ].count += 12;
								this.wordMap[ token ].weight += 2;
							}

						}
						else
						{
							this.wordMap[ token ].count += 1;
						}

						// Process stopwords in string
						if ( stopwords && stopwords.constructor === Array )
						{

							const isStopWord = _.indexOf( stopwords, token );

							if ( isStopWord === -1 )
							{

								if ( this.wordMap[ token ] )
								{

									this.wordMap[ token ].count += 1;

									this.wordMap[ token ].rest = true;

									this.wordMap[ token ].weight += 0.05;

								}

								this.rest.push( token );

							}
							else if ( isStopWord > -1 )
							{

								if ( this.wordMap[ token ] )
								{

									this.wordMap[ token ].weight += 0.10;

									this.wordMap[ token ].stopword = true;

								}

								this.stopwords.push( token );

							}

						}

						// Add token to list of token after all above is filtered.
						this.tokens.push( token );

						// Add stem to list of token after all above is filtered.
						this.stems.push( stem );

						return next();

					}, complete );

				},

				( res, complete ) => {

					async.each( this.parameters, ( fn, cb ) => {

						fn( {
							rest: () => ( ar.join(' ') ),
							text: ar.join(' '),
							tokens: ar
						}, ( err, res ) => {

							if ( err )
							{
								return cb( err );
							}

							if ( ! res )
							{
								return cb();
							}

							if ( res )
							{

								if ( res.constructor === Object )
								{
									this.tagger[ res.tag ] =  _.merge( this.tagger[ res.tag ], res );
									return;
								}

								if ( res.constructor === Array )
								{

									if ( res.length === 0 ) { return; }

									if ( ! this.tagger[ res[ 0 ].tag ] )
									{
										this.tagger[ res[ 0 ].tag ] = res;
									}

									else
									{
										this.tagger[ res[ 0 ].tag ] = this.tagger[ res[ 0 ].tag ].concat( res );
									}

								}
								else
								{

									if ( ! this.tagger[ res.tag ] )
									{
										this.tagger[ res.tag ] = [ res ];
									}

									else
									{
										this.tagger[ res.tag ].push( res );
									}

								}

							}

							return cb();

						} );

					}, complete );

				},

			], ( err ) => {

				if ( err )
				{
					return done( err );
				}

				return done();

			} );

		} );

		// Add bigrams
		this.bigrams = natural.NGrams.bigrams( this.cleanSTR );

		// Call any string plug-ins, passes a callback.
		// this is bind to the function being called.
		// this will stop execution inside the plug-in, be careful.
		// unless the callback is called properly.
		async.each( this.plugins.string, ( stringPlugin, pluginCallback ) => stringPlugin.call( this, pluginCallback ), complete );

	}

	addParameter ( fn ) {

		'use strict';

		if ( ! fn || ( fn.constructor === Function ) === false )
		{
			throw new Error('Param is not a function');
		}

		this.parameters.push( fn.bind( this ) );

	}

	study ( data, callback ) {

		'use strict';

		const logicStudy = {};

		// this helps tell async.auto what is the next in chain...
		// used below. right before checking for twitter and processing it.
		let last = 'emojiSentiment';

		logicStudy.str = ( done ) => {

			let str;

			if ( data.constructor === String )
			{

				str = data;

			}
			else if ( data.constructor === Object )
			{

				if ( data.text )
				{
					str = data.text.toString();
				}
				else if ( data.string )
				{
					str = data.string;
				}

			}

			if ( str.length < 1 )
			{
				return done( true );
			}

			str = str.toLowerCase();

			return done( null, str );

		};

		logicStudy.lang = [ 'str', ( res, done ) => {

			if ( this.franc )
			{

				const lang = _.first( franc.all( res.str, {
					minLength: 4,
					whitelist: this.lang.whitelist
				} ), this.lang.whitelist.length );

				this.lang.guess = lang[ 0 ];

				return done( null, this.lang.guess );

			}
			else if ( this.languagedetect )
			{

				const lang = lngDetector.detect( res.str );

				if ( lang[ 0 ][ 0 ] === 'english' )
				{
					this.lang.guess = 'eng';
				}

				else if ( lang[ 0 ][ 0 ] === 'spanish' )
				{
					this.lang.guess = 'spa';
				}

				else if ( lang[ 0 ][ 0 ] === 'french' )
				{
					this.lang.guess = 'fra';
				}

				else if ( lang[ 0 ][ 0 ] === 'portuguese' )
				{
					this.lang.guess = 'por';
				}

				return done( null, this.lang.guess );

			}
			else
			{
				return done( null, this.lang || null );
			}

		} ];

		logicStudy.stringProcess = [ 'lang', ( res, done ) => this.stringProcess( res, done ) ];

		// early and dumbest topics picker...
		// useful later on...
		logicStudy.topics = [ 'stringProcess', ( res, done ) => {

			_.each( this.wordMap, ( word, next ) => {

				if ( word.length < 3 ) { return; }
				if ( word.rest ) { return; }

				this.topics.push( word.text );

			} );

			done( null, this.topics );

		} ];

		logicStudy.tagger = [ 'topics', ( res, done ) => done( null, this.tagger ) ];

		// logicStudy.rest = [ 'tagger', ( res, done ) => done( null, this.rest ) ];

		// this only works in Spanish currently...
		// TODO: add English to this.
		logicStudy.dal = [ 'tagger', ( res, done ) => {

			if ( this.lang.guess === 'spa' )
			{

				const sdal = require( __dirname + '/../data/sdal-spanish.json' );

				const sdalMap = {

					totals:
					{
						activation: 0,
						imagination: 0,
						pleasantness: 0,
					},

					words: {},

					wordCount: 0,

				};

				async.each( this.tokens, ( word, next ) => {

					if ( sdal[ word ] && sdalMap.words[ word ] === undefined )
					{

						sdalMap.words[ word ] = {
							count: 1,
							obj: sdal[ word ].obj,
							activation: _.floor( + sdal[ word ].activation, 2 ),
							imagination: _.floor( + sdal[ word ].imagination, 2 ),
							pleasantness: _.floor( + sdal[ word ].pleasantness, 2 ),
						};

						sdalMap.totals.activation += + sdal[ word ].activation;
						sdalMap.totals.imagination += + sdal[ word ].imagination;
						sdalMap.totals.pleasantness += + sdal[ word ].pleasantness;

						sdalMap.wordCount += 1;

						this.topics.push( word );

						switch ( sdal[ word ].obj )
						{

							case 'V':
								if ( ! sdalMap.verbs ) { sdalMap.verbs = []; }
								sdalMap.verbs.push( word );
								if ( this.wordMap[ word ] )
								{
									this.wordMap[ word ].count += 5;
									this.wordMap[ word ].weight += 0.5;
								}
							break;

							case 'A':
								if ( ! sdalMap.adjectives ) { sdalMap.adjectives = []; }
								sdalMap.adjectives.push( word );
								if ( this.wordMap[ word ] )
								{
									this.wordMap[ word ].count += 4;
									this.wordMap[ word ].weight += 0.4;
								}
							break;

							case 'N':
								if ( ! sdalMap.nouns ) { sdalMap.nouns = []; }
								sdalMap.nouns.push( word );
								if ( this.wordMap[ word ] )
								{
									this.wordMap[ word ].count += 3;
									this.wordMap[ word ].weight += 0.3;
								}
							break;

						}

					}

					else if ( sdal[ word ] && sdalMap.words[ word ] )
					{

						sdalMap.words[ word ].count += 1;

						sdalMap.totals.activation += + sdal[ word ].activation;
						sdalMap.totals.imagination += + sdal[ word ].imagination;
						sdalMap.totals.pleasantness += + sdal[ word ].pleasantness;

					}

					return next();

				}, () => {

					if ( sdalMap.wordCount > 0 )
					{
						sdalMap.totals.activation = _.floor( sdalMap.totals.activation / sdalMap.wordCount, 2 );
						sdalMap.totals.imagination = _.floor( sdalMap.totals.imagination / sdalMap.wordCount, 2 );
						sdalMap.totals.pleasantness = _.floor( sdalMap.totals.pleasantness / sdalMap.wordCount, 2 );
					}

					return done( null, sdalMap );

				} );

			}

			return done();

		} ];

		logicStudy.sentiment = [ 'tagger', ( res, done ) => {

			const sentWordsFound = [];

			const sentiment =
			{
				polarity: 0,
				positive:
				{
					score: 0,
					words: []
				},
				negative:
				{
					score: 0,
					words: []
				}
			};

			const checkWordExist = ( word ) => {

				let out = false;

				_.each( sentWordsFound, ( item ) => {

					if ( _.indexOf( item, word ) > -1 )
					{

						out = true;

						return;
					}

				} );

				return out;

			};

			const evaluateSentimen = ( word ) => {

				if ( this.afinn[ res.lang ] === undefined ) { return; }

				if ( word.length < 3 ) { return; }

				if ( checkWordExist( word ) === true ) { return; }

				if ( this.ignore && _.indexOf( this.ignore, word ) > -1 ) { return; }

				const value = this.afinn[ res.lang ][ word ];

				if ( value && this.wordMap[ word ] )
				{
					this.wordMap[ word ].sentiment = value;
					this.wordMap[ word ].count += 2;
				}

				if ( value > 0 )
				{

					if ( _.indexOf( sentiment.positive.words, word ) === -1 )
					{

						sentiment.positive.score += value;

						sentiment.positive.words.push( word );

						sentWordsFound.push( word );

						this.topics.push( word );

						sentiment.polarity += value;

					}

				}
				else if ( value < 0 )
				{

					if ( _.indexOf( sentiment.negative.words, word ) === -1 )
					{

						sentiment.negative.score += value;

						sentiment.negative.words.push( word );

						sentWordsFound.push( word );

						this.topics.push( word );

						sentiment.polarity += value;

					}

				}

			};

			async.parallel( [

				( cb ) => {

					async.each( this.trigrams, ( ar, next ) => {

						const word = ar.join( ' ' );

						if ( this.afinn[ this.lang.guess ] && this.afinn[ this.lang.guess ][ word ] )
						{
							evaluateSentimen( word );
						}

						return next();

					}, cb );

				},

				( cb ) => {

					async.each( this.bigrams, ( ar, next ) => {

						const word = ar.join( ' ' );

						if ( this.afinn[ this.lang.guess ] && this.afinn[ this.lang.guess ][ word ] )
						{
							evaluateSentimen( word );
						}

						return next();

					}, cb );

				},

				( cb ) => {

					async.each( this.tokens, ( token, next ) => {

						evaluateSentimen( token );

						return next();

					}, cb );

				},

				( cb ) => {

					async.each( this.stems, ( stem, next ) => {

						evaluateSentimen( stem );

						return next();

					}, cb );

				}

			], () => {

				async.eachOf( sentiment, ( obj, sentimentKind, next ) => {

					async.each( obj.words, ( word, nxt ) => {

						const index = _.indexOf( this.rest, word );

						if ( index > -1 )
						{
							delete this.rest[ index ];
						}

						nxt();

					}, next );

				}, (  ) => done( null, sentiment ) );

			} );

		} ];

		logicStudy.emojiSentiment = [ 'tagger', ( res, done ) => {

			const sentEmojiFound = [];

			const emojiSentiment =
			{
				polarity: 0,
				positive:
				{
					score: 0,
					emoji: []
				},
				negative:
				{
					score: 0,
					emoji: []
				}
			};

			const chars = res.str
				.replace(/[\s]*[\w]*[\.\,\?\\\!]*[\d]*/igm,'')
				.split( /([\uD800-\uDBFF][\uDC00-\uDFFF])/igm );

			async.each( chars, ( char, next ) => {

					async.each( emojiEmotion, ( emoji, nxt ) => {

						if ( char === emoji.emoji && _.indexOf( sentEmojiFound, char ) === -1 )
						{

							if ( emoji.polarity > 0 )
							{
								emojiSentiment.positive.score += emoji.polarity;
								emojiSentiment.positive.emoji.push( emoji.emoji );
							}

							else if ( emoji.polarity < 0 )
							{
								emojiSentiment.negative.score += emoji.polarity;
								emojiSentiment.negative.emoji.push( emoji.emoji );
							}

							emojiSentiment.polarity += emoji.polarity;

							sentEmojiFound.push( char );

						}

						return nxt();

					}, next );

				}, () => {

					return done( null, emojiSentiment );

				} );

		} ];

		if ( this.twitter !== false )
		{

			last = 'twitter';

			logicStudy.twitter = [ 'emojiSentiment', ( res, done ) => {

				const out =
				{
					parsedAt: Date.now()
				};

				out.mentions = twitterTX.extractMentions( res.str ) || [];
				out.hashtags = twitterTX.extractHashtags( res.str ) || [];
				out.cashtags = twitterTX.extractCashtags( res.str ) || [];
				out.replies = twitterTX.extractReplies( res.str ) || [];
				out.urls = twitterTX.extractUrls( res.str ) || [];

				async.eachOf( this.wordMap, ( obj, word, next ) => {

					async.parallel( [

						( cb ) => {

							async.each( out.mentions, ( mention, nxt ) => {

								if ( this.itemStem( word ).indexOf( mention ) > -1 )
								{

									this.wordMap[ word ].mention = true;

									this.wordMap[ word ].count += 3;

									const index = this.rest.indexOf( word );

									if ( index > -1 )
									{
										delete this.rest[ index ];
									}

								}

								return nxt();

							}, cb );

						},

						( cb ) => {

							async.each( out.hashtags, ( hashtag, nxt ) => {

								if ( word.indexOf( hashtag ) > -1 )
								{

									this.wordMap[ word ].hashtag = true;

									this.wordMap[ word ].count += 2;

									const index = this.rest.indexOf( word );

									if ( index > -1 )
									{
										delete this.rest[ index ];
									}

								}

								return nxt();

							}, cb );

						},

						( cb ) => {

							async.each( out.cashtags, ( cashtag, nxt ) => {

								if ( word.indexOf( cashtag ) > -1 )
								{

									this.wordMap[ word ].cashtag = true;

									this.wordMap[ word ].count += 1;

									const index = this.rest.indexOf( word );

									if ( index > -1 )
									{
										delete this.rest[ index ];
									}

								}

								return nxt();

							}, cb );

						}

					], next );

				}, (  ) => {

					if ( data.constructor === Object )
					{

						if ( data.lang )
						{

							if ( data.lang === 'es' )
							{
								data.lang = 'spa';
							}
							else if ( data.lang === 'en' )
							{
								data.lang = 'eng';
							}

							out.twitterLang = data.lang;

						}

						if ( data.created_at ) // jshint ignore:line
						{
							out.createdAt = data.created_at; // jshint ignore:line
						}

						if ( data.user )
						{

							out.user = {};

							if ( data.user.id_str ) // jshint ignore:line
							{
								out.user.id = data.user.id_str; // jshint ignore:line
							}

							if ( data.user.screen_name ) // jshint ignore:line
							{
								out.user.username = data.user.screen_name; // jshint ignore:line
							}

							if ( data.user.name )
							{
								out.user.name = data.user.name;
							}

							if ( data.user.location )
							{
								out.user.location = data.user.location;
							}

							if ( data.user.time_zone ) // jshint ignore:line
							{
								out.user.time_zone = data.user.time_zone; // jshint ignore:line
							}

							if ( data.user.followers_count ) // jshint ignore:line
							{
								out.user.followers = data.user.followers_count; // jshint ignore:line
							}

							if ( data.user.friends_count ) // jshint ignore:line
							{
								out.user.friends = data.user.friends_count; // jshint ignore:line
							}

							if ( data.user.statuses_count ) // jshint ignore:line
							{
								out.user.posts = data.user.statuses_count; // jshint ignore:line
							}

							out.user.gender = gender.guess( out.user.name ).gender;

							if ( ! data.retweeted_status && this.tagger ) // jshint ignore:line
							{

								if ( this.tagger.topic )
								{
									out.user.topics = this.tagger.topic;
								}

								if ( this.tagger.bad )
								{
									out.user.bad = this.tagger.bad;
								}

								if ( this.tagger.good )
								{
									out.user.good = this.tagger.good;
								}

								if ( this.tagger.interesting )
								{

									out.user.topics = _.chain( out.user.topics )
										.concat( this.tagger.interesting )
										.uniq()
										.value();
								}

							}
							else
							{

								out.user.topics = this.topics;

							}

							out.user.topics = _.chain( this.topics )
								.concat( out.user.topics )
								.uniq()
								.value();

							out.user.tagger = this.tagger;

							out.user.hashtags = out.hashtags;

							out.user.mentions = out.mentions;

							out.user.parsedAt = Date.now();

							if ( res.sentiment )
							{
								out.user.polarity = res.sentiment.polarity;
							}

						}

						if ( data.retweeted_status ) // jshint ignore:line
						{

							out.isRepost = true; // jshint ignore:line

							if ( data.retweeted_status.user ) // jshint ignore:line
							{

								out.originalBy = {};

								if ( data.retweeted_status.user.id_str ) // jshint ignore:line
								{
									out.originalBy.id = data.retweeted_status.user.id_str; // jshint ignore:line
								}

								if ( data.retweeted_status.user.screen_name ) // jshint ignore:line
								{
									out.originalBy.username = data.retweeted_status.user.screen_name; // jshint ignore:line
								}

								if ( data.retweeted_status.user.name ) // jshint ignore:line
								{
									out.originalBy.name = data.retweeted_status.user.name; // jshint ignore:line
								}

								if ( data.retweeted_status.user.location ) // jshint ignore:line
								{
									out.originalBy.location = data.retweeted_status.user.location; // jshint ignore:line
								}

								if ( data.retweeted_status.user.time_zone ) // jshint ignore:line
								{
									out.originalBy.time_zone = data.retweeted_status.user.time_zone; // jshint ignore:line
								}

								if ( data.retweeted_status.user.followers_count ) // jshint ignore:line
								{
									out.originalBy.followers = data.retweeted_status.user.followers_count; // jshint ignore:line
								}

								if ( data.retweeted_status.user.friends_count ) // jshint ignore:line
								{
									out.originalBy.friends = data.retweeted_status.user.friends_count; // jshint ignore:line
								}

								if ( data.retweeted_status.user.statuses_count ) // jshint ignore:line
								{
									out.originalBy.posts = data.retweeted_status.user.statuses_count; // jshint ignore:line
								}

								out.originalBy.gender = gender.guess( out.originalBy.name ).gender;

								if ( this.tagger )
								{

									if ( this.tagger.topic )
									{
										out.originalBy.topics = this.tagger.topic;
									}

									if ( this.tagger.interesting )
									{

										out.originalBy.topics = _.chain( out.originalBy.topics )
											.concat( this.tagger.interesting )
											.uniq()
											.value();
									}

									out.originalBy.tagger = this.tagger;

								}
								else
								{

									out.originalBy.topics = _.take( this.topics, 5 );

								}

								if ( out.hashtags )
								{

									out.originalBy.topics = _.chain( out.originalBy.topics )
										.concat( _.map( out.hashtags, ( hash ) => ( '#' + hash ) ) )
										.uniq()
										.value();
								}

								out.originalBy.hashtags = out.hashtags;

								out.originalBy.topics = _.chain( this.topics )
									.concat( out.originalBy.topics )
									.uniq()
									.value();

								out.originalBy.tagger = this.tagger;

								out.originalBy.hashtags = out.hashtags;

								out.originalBy.mentions = out.mentions;

								out.originalBy.parsedAt = Date.now();

								if ( res.sentiment )
								{
									out.originalBy.polarity = res.sentiment.polarity;
								}

							}

						}

					}

					return done( null, out );

				} );

			} ];

		}

		logicStudy.wordMap = [ last, ( res, done ) => {

			res.topics = [];

			const size = _.size( this.wordMap );

			async.eachOf( this.wordMap, ( item, key, next ) => {

				if ( this.wordMap[ key ].stopword )
				{
					this.wordMap[ key ].weight -= 0.15;
				}

				if ( this.wordMap[ key ].rest )
				{
					this.wordMap[ key ].weight -= 0.1;
				}

				if ( this.wordMap[ key ].sentiment )
				{
					this.wordMap[ key ].weight += 0.3;
				}

				if ( this.wordMap[ key ].mention )
				{
					this.wordMap[ key ].weight += 0.2;
				}

				if ( this.wordMap[ key ].hashtag )
				{
					this.wordMap[ key ].weight += 0.1;
				}

				if ( this.wordMap[ key ].good )
				{
					this.wordMap[ key ].weight += 0.35;
				}

				if ( this.wordMap[ key ].bad )
				{
					this.wordMap[ key ].weight += 0.35;
				}

				if ( this.wordMap[ key ].interesting )
				{
					this.wordMap[ key ].weight += 0.65;
				}

				if ( this.wordMap[ key ].topic )
				{
					this.wordMap[ key ].weight += 0.60;
				}

				this.wordMap[ key ].weight = ( item.count + this.wordMap[ key ].weight ) / size;

				this.wordMap[ key ].weight = _.floor( this.wordMap[ key ].weight, 2 );

				// parse topics
				// if ( this.wordMap[ key ].mention ) { return; }
				// if ( this.wordMap[ key ].hashtag ) { return; }
				// if ( this.wordMap[ key ].cashtag ) { return; }

				// if ( this.target && this.target.indexOf( item.stem ) > -1 ) { return; }
				// if ( this.interesting && this.interesting.indexOf( item.stem ) > -1 ) { return; }

				if ( this.wordMap[ key ].weight >= 0.50 )
				{
					res.topics.push( [ key, item ] );
				}

				return next();

			}, () => {

				res.topics = _.chain( res.topics )
					.map( ( topic ) => ( topic[ 1 ] ) )
					.orderBy( 'weight', 'desc' )
					.value();

				// update Twitter users topics, this works thanks to async
				if ( res.twitter )
				{

					if ( res.twitter.user )
					{
						res.twitter.user.topics = res.topics;
					}

					if ( res.twitter.originalBy )
					{
						res.twitter.originalBy.topics = res.topics;
					}

				}

				return done( null, this.wordMap );

			} );

		} ];

		async
			.auto( logicStudy, ( err, res ) => {

				delete res.afterGuess;
				delete res.stringProcess;
				delete res.filterTopics;
				delete res.parseUsers;

				callback( err, res );

				this.rest = [];
				this.tagger = {};
				this.topics = [];
				this.stopwords = [];
				this.stems = [];
				this.tokens = [];
				this.bigrams = [];
				this.trigrams = [];
				this.wordMap = {};

				return;

			} );

	}

}

module.exports = Barfer;

// EOF
