
const _ = require( 'lodash' );

const assert = require( 'assert' );

const path = require( 'path' );

const emojiEmotion = require( 'emoji-emotion' );

const Barfer = require( './../lib/' );

const twitterData = require( './fixtures/twitter.data.json' ).list;

if ( process.env.VERBOSE )
{
	console.log( `Working with ${twitterData.length} entries.\n` );
	console.log( 'Starting tests...\n' );
}

const barfer = new Barfer( {
	clean: true,
	latinize: true,
	twitter: true,
	lang:
	{
		whitelist: [ 'eng' ]
	},
} );

const outMap = {
	polarity: 0,
	sentiment: {},
	topics: {
		words: {},
		top3: [],
		top20: [],
		top50: [],
	},
	mentions: {},
	hashtags: {},
	tagger: {},
};

_.each( twitterData, ( str, key ) => {

	if ( process.env.VERBOSE )
	{

		if ( key % 10 === 0 )
		{
			console.log( `Processed entries ${key}-${key+10}.` );
		}

	}

	barfer
		.study( str, ( err, data ) => {

			if ( process.env.DEBUG )
			{
				console.log( require('util').inspect( data, { depth: null, colors: true, showHidden: true } ) );
				// console.log( require('util').inspect( data.tagger, { depth: null, colors: true, showHidden: true } ) );
			}

			if ( key === 0 )
			{
				// First string:
				// 'rt @jonbershad: @realdonaldtrump fun. so you won\'t be giving us a date when you\'ll be discussing your massive conflicts of interest?'
				// console.log( require('util').inspect( data, { depth: null, colors: true, showHidden: true } ) );
				assert.equal( data.topics[0].text, 'giving' );
				assert.equal( data.topics[1].text, 'fun' );
				assert.equal( data.topics[2].text, 'discussing' );
				assert.equal( data.topics[3].text, 'won' );
				assert.equal( data.topics[4].text, 'conflicts' );
				assert.equal( data.topics[5].text, 'interest' );
				assert.equal( data.sentiment.polarity, 4 );
				assert.equal( data.sentiment.positive.score, 8 );
				assert.equal( data.sentiment.negative.score, -4 );
			}

			if ( key === 2 )
			{
				// Third string:
				// 'rt @ingrahamangle: watching his approach to ryan, romney, perry &amp; fiorina, we see a magnanimous, pragmatic &amp; forgiving @realdonaldtrump.'
				assert.equal( data.topics[0].text, 'watching' );
				assert.equal( data.topics[1].text, 'approach' );
				assert.equal( data.topics[2].text, 'forgiving' );
				assert.equal( data.sentiment.polarity, 1 );
				assert.equal( data.sentiment.positive.score, 1 );
				assert.equal( data.sentiment.negative.score, 0 );
			}

			outMap.polarity += data.sentiment.polarity;

			if ( data.tagger )
			{

				_.each( data.tagger, ( tagger, key ) => {

					if ( outMap.tagger[ key ] === undefined )
					{
						outMap.tagger[ key ] = {};
					}

					_.each( tagger.words, ( item ) => {

						if ( outMap.tagger[ key ][ item.text ] )
						{
							outMap.tagger[ key ][ item.text ].count += 1
						}

						else
						{

							outMap.tagger[ key ][ item.text ] = {
								count: 1,
								tagger: key,
								text: item.text,
							};

						}

					} );

				} );

			}

			if ( data.twitter )
			{

				_.each( data.twitter.mentions, ( mention ) => {

					if ( outMap.mentions[ mention ] )
					{
						outMap.mentions[ mention ].count += 1;
					}

					else
					{

						outMap.mentions[ mention ] = {
							count: 1,
							mention,
						};

					}

				} );

				_.each( data.twitter.hashtags, ( hashtag ) => {

					if ( outMap.hashtags[ hashtag ] )
					{
						outMap.hashtags[ hashtag ].count += 1;
					}

					else
					{

						outMap.hashtags[ hashtag ] = {
							count: 1,
							hashtag,
						};

					}

				} );

			}

			_.each( data.topics, ( item ) => {

				if ( outMap.topics.words[ item.text ] )
				{
					outMap.topics.words[ item.text ].count += 1;
				}

				else
				{

					outMap.topics.words[ item.text ] = {
						count: 1,
						text: item.text,
					};

					if ( item.sentiment )
					{

						outMap.topics.words[ item.text ].sentiment = item.sentiment;

						if ( outMap.sentiment[ item.text ] )
						{
							outMap.sentiment[ item.text ].count += 1;
							outMap.sentiment[ item.text ].sentiment += item.sentiment;
						}
						else
						{
							outMap.sentiment[ item.text ] = outMap.topics.words[ item.text ];
							if ( item.negative )
							{
								outMap.sentiment[ item.text ].negative = true;
							}
							if ( item.positive )
							{
								outMap.sentiment[ item.text ].positive = true;
							}
						}

					}

				}

			} );

		} );

} );

outMap.sentiment = {
	positive: _.take( _.orderBy( outMap.sentiment, 'sentiment', 'desc' ), _.size( outMap.sentiment ) / 3 ),
	negative: _.take( _.orderBy( outMap.sentiment, 'sentiment', 'asc' ), _.size( outMap.sentiment ) / 3 ),
};

outMap.tagger.actions = _.take( _.orderBy( _.map( outMap.tagger.actions, ( item ) => { return item; } ), 'count', 'desc' ), 10 );
outMap.tagger.topics = _.take( _.orderBy( _.map( outMap.tagger.topics, ( item ) => { return item; } ), 'count', 'desc' ), 10 );
outMap.tagger.positive = _.take( _.orderBy( _.map( outMap.tagger.positive, ( item ) => { return item; } ), 'count', 'desc' ), 10 );
outMap.tagger.negative = _.take( _.orderBy( _.map( outMap.tagger.negative, ( item ) => { return item; } ), 'count', 'desc' ), 10 );
outMap.tagger.institutions = _.take( _.orderBy( _.map( outMap.tagger.institutions, ( item ) => { return item; } ), 'count', 'desc' ), 10 );
outMap.tagger.sort = _.take( _.orderBy( _.map( outMap.tagger.sort, ( item ) => { return item; } ), 'count', 'desc' ), 10 );
outMap.tagger.places = _.take( _.orderBy( _.map( outMap.tagger.places, ( item ) => { return item; } ), 'count', 'desc' ), 10 );
outMap.tagger.label = _.take( _.orderBy( _.map( outMap.tagger.label, ( item ) => { return item; } ), 'count', 'desc' ), 10 );
outMap.tagger.hesitate = _.take( _.orderBy( _.map( outMap.tagger.hesitate, ( item ) => { return item; } ), 'count', 'desc' ), 10 );

outMap.sentiment.negative = _.take( _.orderBy( outMap.sentiment.negative, 'count', 'desc' ), 20 );
outMap.sentiment.positive = _.take( _.orderBy( outMap.sentiment.positive, 'count', 'desc' ), 20 );

// Twitter
outMap.mentions = _.take( _.orderBy( outMap.mentions, 'count', 'desc' ), 10 );
outMap.hashtags = _.take( _.orderBy( outMap.hashtags, 'count', 'desc' ), 10 );

outMap.topics.top50 = _.take( _.orderBy( outMap.topics.words, 'count', 'desc' ), 50 );
outMap.topics.top3 = _.take( outMap.topics.top50, 3 );
outMap.topics.top20 = _.take( outMap.topics.top50, 20 );

delete outMap.topics.top50;
delete outMap.topics.words;

if ( process.env.VERBOSE )
{

	if ( process.env.DEBUG )
	{
		console.log( require('util').inspect( outMap, { depth: null, colors: true, showHidden: true } ) );
	}

}

assert.equal( outMap.polarity, 63 );

assert.deepEqual( outMap.topics.top3, [
		{ count: 106, text: 'realdonaldtrump' },
		{ count: 13, text: 'elect' },
		{ count: 9, text: 'thank', sentiment: 2, positive: true },
	] );

assert.deepEqual( outMap.topics.top20, [
		{ count: 106, text: 'realdonaldtrump' },
		{ count: 13, text: 'elect' },
		{ count: 9, text: 'thank', sentiment: 2, positive: true },
		{ count: 8, text: 'great', sentiment: 3, positive: true },
		{ count: 7, text: 'want', sentiment: 1, positive: true },
		{ count: 6, text: 'help', sentiment: 2, positive: true },
		{ count: 6, text: 'governorperry' },
		{ count: 6, text: 'watch' },
		{ count: 6, text: 'speakerryan' },
		{ count: 5, text: 'president' },
		{ count: 5, text: 'kanyewest' },
		{ count: 4, text: 'won', sentiment: 3, positive: true },
		{ count: 4, text: 'work' },
		{ count: 4, text: 'to' },
		{ count: 4, text: 'lost', sentiment: -3, negative: true },
		{ count: 4, text: 'best', sentiment: 3, positive: true },
		{ count: 4, text: 'like', sentiment: 2, positive: true },
		{ count: 4, text: 'merry', sentiment: 3, positive: true },
		{ count: 4, text: 'wisconsin' },
		{ count: 4, text: 'good', sentiment: 3, positive: true },
	] );

assert.deepEqual( outMap.mentions, [
		{ count: 182, mention: 'realdonaldtrump' },
		{ count: 21, mention: 'foxnews' },
		{ count: 9, mention: 'mike_pence' },
		{ count: 9, mention: 'kanyewest' },
		{ count: 8, mention: 'speakerryan' },
		{ count: 6, mention: 'governorperry' },
		{ count: 6, mention: 'scottwalker' },
		{ count: 6, mention: 'gop' },
		{ count: 5, mention: 'politico' },
		{ count: 4, mention: 'cnn' },
	] );

assert.deepEqual( outMap.hashtags, [
		{ count: 5, hashtag: 'maga' },
		{ count: 3, hashtag: 'makeamericagreatagain' },
		{ count: 2, hashtag: 'russiahacking' },
		{ count: 2, hashtag: 'russianinterference' },
		{ count: 2, hashtag: 'sandyhook' },
		{ count: 2, hashtag: 'thankyoutour2016' },
		{ count: 2, hashtag: 'trump' },
		{ count: 2, hashtag: 'informtheelectors' },
		{ count: 2, hashtag: 'tcot' },
		{ count: 2, hashtag: 'badforamerica' },
	] );

assert.equal( outMap.sentiment.positive[ 0 ].text, 'thank' );
assert.equal( outMap.sentiment.positive[ 0 ].count, 9 );

assert.equal( outMap.sentiment.positive[ 1 ].text, 'great' );
assert.equal( outMap.sentiment.positive[ 1 ].count, 8 );

assert.equal( outMap.sentiment.positive[ 2 ].text, 'help' );
assert.equal( outMap.sentiment.positive[ 2 ].count, 6 );

assert.equal( outMap.sentiment.positive[ 3 ].text, 'won' );
assert.equal( outMap.sentiment.positive[ 3 ].count, 4 );

assert.equal( outMap.sentiment.positive[ 4 ].text, 'best' );
assert.equal( outMap.sentiment.positive[ 4 ].count, 4 );

assert.equal( outMap.sentiment.negative[ 0 ].text, 'lost' );
assert.equal( outMap.sentiment.negative[ 0 ].count, 4 );

assert.equal( outMap.sentiment.negative[ 1 ].text, 'wrong' );
assert.equal( outMap.sentiment.negative[ 1 ].count, 3 );

assert.equal( outMap.sentiment.negative[ 2 ].text, 'fuck' );
assert.equal( outMap.sentiment.negative[ 2 ].count, 2 );

assert.equal( outMap.sentiment.negative[ 3 ].text, 'crime' );
assert.equal( outMap.sentiment.negative[ 3 ].count, 2 );

assert.equal( outMap.sentiment.negative[ 4 ].text, 'fake' );
assert.equal( outMap.sentiment.negative[ 4 ].count, 2 );

assert.equal( outMap.tagger.actions[ 0 ].count, 13 );

assert.equal( outMap.tagger.topics[ 0 ].count, 181 );

assert.equal( outMap.tagger.positive[ 0 ].count, 9 );

assert.equal( outMap.tagger.negative[ 0 ].count, 4 );

assert.equal( outMap.tagger.sort[ 0 ].count, 1 );
assert.equal( outMap.tagger.sort.length, 2 );

assert.equal( outMap.tagger.places[ 0 ].count, 1 );
assert.equal( outMap.tagger.places.length, 3 );

assert.equal( outMap.tagger.institutions[ 0 ].count, 2 );
assert.equal( outMap.tagger.institutions.length, 4 );

assert.equal( outMap.tagger.label[ 0 ].count, 1 );
assert.equal( outMap.tagger.label.length, 2 );

// assert.equal( outMap.tagger.hesitate[ 0 ].count, 1 );
// assert.equal( outMap.tagger.hesitate.length, 1 );

if ( process.env.VERBOSE )
{

	console.log( '\nAll tests passed correctly\n' );

}

// EOF